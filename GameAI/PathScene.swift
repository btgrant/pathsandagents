//
//  PathScene.swift
//  GameAI
//
//  Created by Josh Smith on 3/23/16.
//  Copyright © 2016 Josh Smith. All rights reserved.
//

import SpriteKit
import GameplayKit

class PlacingObstacles: GKState {}
class PlacingTarget: GKState {}

protocol PSButton {
    func tapped(touch: UITouch) -> Bool;
}

class PSObstacleButton: SKSpriteNode, PSButton {
    func tapped(touch: UITouch) -> Bool {
        let loc = touch.locationInNode(self.parent!)
        return self.containsPoint(loc);
    }
}

class PSTargetButton : SKSpriteNode, PSButton {
    func tapped(touch: UITouch) -> Bool {
        let loc = touch.locationInNode(self.parent!)
        return self.containsPoint(loc);
    }
}

let indicatorAgent = GKAgent2D()
let seekButton = GKGoal(toInterceptAgent: indicatorAgent, maxPredictionTime: 1)

class ButtonIndicator: SKSpriteNode, GKAgentDelegate {
    let myAgent = GKAgent2D()
    
    func setupAgent() {
        myAgent.delegate = self
        myAgent.behavior = GKBehavior(goals: [seekButton], andWeights: [100])
        myAgent.maxSpeed = 250
        myAgent.maxAcceleration = 250
    }
    
    func agentDidUpdate(agent: GKAgent) {
        self.position = CGPoint(x: CGFloat(self.myAgent.position.x), y: CGFloat(self.myAgent.position.y))
    }
    
    func agentWillUpdate(agent: GKAgent) {
        // you have to have this method implemented
    }

}

class PathShip: SKSpriteNode, GKAgentDelegate {
    let agent = GKAgent2D()
    
    class func newShip(size: CGSize, position: CGPoint) -> ShipNode {
        let ship = ShipNode(imageNamed: "Spaceship")
        ship.size = size
        ship.position = position
        ship.agent.delegate = ship
        ship.agent.maxSpeed = 100
        ship.agent.maxAcceleration = 50
        ship.agent.position = vector2(Float(ship.position.x), Float(ship.position.y))
        return ship
    }
    
    func agentDidUpdate(agent: GKAgent) {
        let two_d = agent as! GKAgent2D
        self.position = CGPoint(x: CGFloat(two_d.position.x), y: CGFloat(two_d.position.y))
        self.zRotation = CGFloat(Double(two_d.rotation) - M_PI_4)
    }
    
    func agentWillUpdate(agent: GKAgent) {
        // you must implement this
    }
}

class PathScene: SKScene {
    let state = GKStateMachine(states: [ PlacingObstacles(), PlacingTarget()])
    let selectingIndicator = ButtonIndicator(color: SKColor.blackColor(), size: CGSizeMake(70, 70))
    
    var obstacleButton: PSObstacleButton? = nil
    var targetButton: PSTargetButton? = nil

    var lastTime:NSTimeInterval = 0
    
    let ship = ShipNode.newShip(CGSizeMake(50,50), position: CGPointMake(100, 100))
    
    override func didMoveToView(view: SKView) {
        self.addChild(self.ship)
        
        state.enterState(PlacingObstacles.self)
        self.addChild(selectingIndicator)

        self.obstacleButton = self.childNodeWithName("obstacleButton") as? PSObstacleButton
        self.targetButton = self.childNodeWithName("targetButton") as? PSTargetButton

        if let position = self.obstacleButton?.position {
            indicatorAgent.position = vector2(Float(position.x), Float(position.y))
        }
        self.selectingIndicator.setupAgent()
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.locationInNode(self)
            if self.obstacleButton!.tapped(touch) {
                self.state.enterState(PlacingObstacles.self)
                indicatorAgent.position = vector2(Float(position.x), Float(position.y))
                
            } else if self.targetButton!.tapped(touch) {
                self.state.enterState(PlacingTarget.self)
                indicatorAgent.position = vector2(Float(position.x), Float(position.y))
            } else {
                if let current = self.state.currentState {
                    switch current {
                    case is PlacingObstacles:
                        let obstacle = SKSpriteNode(color: SKColor.blueColor(), size: CGSizeMake(55,55))
                        self.addChild(obstacle)
                        obstacle.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(55, 55))
                        obstacle.physicsBody?.dynamic = false
                        obstacle.position = position
                        obstacle.name = "obstacle"
                    case is PlacingTarget:
                        let target = SKSpriteNode(color: SKColor.redColor(), size: CGSizeMake(55,55))
                        self.addChild(target)
                        target.position = position
                        target.name = "target"
                        moveToTarget(target)
                    default:
                        break
                    }
                }
            }
        }
    }
    
    func moveToTarget(node: SKSpriteNode) {
        let shipNode = GKGraphNode2D(point: vector_float2(Float(ship.position.x), Float(ship.position.y)))
        
        let targetNode = GKGraphNode2D(point: vector_float2(Float(node.position.x), Float(node.position.y)))
        
        //let obstacles = SKNode.obstaclesFromNodeBounds(self["obstacle"])
        let obstacles = SKNode.obstaclesFromNodePhysicsBodies(self["obstacle"])
        
        guard obstacles.count > 0 else {
            self.removeChildrenInArray(self["obstacle"])
            self.removeChildrenInArray(self["target"])
            return
        }
        print(obstacles.count)
        
        let obstacleGraph = GKObstacleGraph(obstacles: obstacles, bufferRadius: 50)
        
        obstacleGraph.connectNodeUsingObstacles(shipNode)
        obstacleGraph.connectNodeUsingObstacles(targetNode)

        
        let graphNodes = obstacleGraph.findPathFromNode(shipNode, toNode: targetNode) as! [GKGraphNode2D]

        let path = GKPath(graphNodes: graphNodes, radius: 1024)
        
        let bzpath = UIBezierPath()
        
        for var i = 0; i < path.numPoints; i++ {
            let nextPoint = path.pointAtIndex(i)
            let nextCGPoint = CGPoint(x: Double(nextPoint.x), y: Double(nextPoint.y))
            if i > 0 {
                bzpath.addLineToPoint(nextCGPoint)
            } else {
                bzpath.moveToPoint(nextCGPoint)
            }
        }
        bzpath.addLineToPoint(node.position)
        
        let actions = [SKAction.followPath(bzpath.CGPath, asOffset: false, orientToPath: true, duration: 5),
            SKAction.runBlock {
               self.removeChildrenInArray(self["obstacle"])
                self.removeChildrenInArray(self["target"])
            }]
        ship.runAction(SKAction.sequence(actions))
    }
    
    override func update(currentTime: NSTimeInterval) {
        guard lastTime > 0 else {
            lastTime = currentTime
            return
        }
        let deltaTime = currentTime - lastTime
        self.lastTime = currentTime
        
        self.state.updateWithDeltaTime(deltaTime)
        self.selectingIndicator.myAgent.updateWithDeltaTime(deltaTime)
        self.ship.agent.updateWithDeltaTime(deltaTime)
    }
}