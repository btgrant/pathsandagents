//
//  GameScene.swift
//  GameAI
//
//  Created by Josh Smith on 3/23/16.
//  Copyright (c) 2016 Josh Smith. All rights reserved.
//

import SpriteKit
import GameplayKit

let wanderingGoal = GKGoal(toWander: 10)

class ShipNode: SKSpriteNode, GKAgentDelegate {
    let agent = GKAgent2D()
    
    class func newShip(size: CGSize, position: CGPoint) -> ShipNode {
        let ship = ShipNode(imageNamed: "Spaceship")
        ship.size = size
        ship.position = position
        ship.agent.delegate = ship
        ship.agent.maxSpeed = 100
        ship.agent.maxAcceleration = 50
        ship.agent.position = vector2(Float(ship.position.x), Float(ship.position.y))
        return ship
    }
    
    func flockWith(nodes: [ShipNode]) {
        let agents = nodes.map { ship in
            return ship.agent
        }
        
        let separate = GKGoal(toSeparateFromAgents: agents, maxDistance: 100, maxAngle: 90)
        let cohere = GKGoal(toCohereWithAgents: agents, maxDistance: 100, maxAngle: 90)
        let align = GKGoal(toAlignWithAgents: agents, maxDistance: 100, maxAngle: 90)
        agent.behavior = GKBehavior(goals: [separate, cohere, align], andWeights: [100,80,120])
        
    }
    
    func fleeFrom(target: GKAgent) {
        self.agent.behavior = GKBehavior(goal: GKGoal(toFleeAgent: target), weight: 1)
    }
    
    func seekTo(target: GKAgent) {
        self.agent.behavior = GKBehavior(goal: GKGoal(toSeekAgent: target), weight: 1)
    }
    
    func intercept(target: GKAgent) {
        self.agent.behavior = GKBehavior(goal: GKGoal(toInterceptAgent: target, maxPredictionTime: 1), weight:100)
    }
    
    func wander() {
        agent.behavior = GKBehavior(goals: [wanderingGoal], andWeights: [100])
    }
    
    func agentDidUpdate(agent: GKAgent) {
        let two_d = agent as! GKAgent2D
        self.position = CGPoint(x: CGFloat(two_d.position.x), y: CGFloat(two_d.position.y))
        self.zRotation = CGFloat(Double(two_d.rotation) - M_PI_4)
    }
    
    func agentWillUpdate(agent: GKAgent) {
        // you must implement this
    }
}

class GameScene: SKScene {
    
    let componentSystem = GKComponentSystem(componentClass: GKAgent2D.self)
    
    var lastTime: CFTimeInterval = 0
    
    override func didMoveToView(view: SKView) {
        let shipSpriteNode = ShipNode.newShip(CGSizeMake(100,100), position: CGPointMake(512,368))
        self.addChild(shipSpriteNode)

        let othership = ShipNode.newShip(CGSizeMake(50,50), position: CGPointMake(100,500))
        self.addChild(othership)

        wander([shipSpriteNode, othership])
        //flock(shipSpriteNode, theFlock: [shipSpriteNode,othership])

        hawkAndSparrow(false)
    }
    
    func flock(head: ShipNode, theFlock: [ShipNode]) {
        
        let _ = theFlock.map { ship in
            ship.flockWith(theFlock)
            if ship != head {
                ship.agent.behavior?.setWeight(1, forGoal: GKGoal(toSeekAgent: head.agent))
            }
            self.componentSystem.addComponent(ship.agent)
        }
        
        head.agent.behavior?.setWeight(100, forGoal: wanderingGoal)
        
    }
    
    func wander(nodes: [ShipNode]) {
        let _ = nodes.map { ship in
                ship.wander()
                self.componentSystem.addComponent(ship.agent)
        }
    }
    
    func hawkAndSparrow(intercept: Bool) {
        let hawk = ShipNode.newShip(CGSizeMake(50,50), position: CGPointMake(500,100))
        let sparrow = ShipNode.newShip(CGSizeMake(25, 25), position: CGPointMake(200,300))

        if intercept {
            hawk.intercept(sparrow.agent)
        } else {
            hawk.seekTo(sparrow.agent)
        }
        
        hawk.agent.maxAcceleration = 200
        hawk.agent.maxSpeed = 200
        
        sparrow.fleeFrom(hawk.agent)
        sparrow.agent.maxSpeed = 50
        
        self.componentSystem.addComponent(hawk.agent)
        self.componentSystem.addComponent(sparrow.agent)
        
        self.addChild(sparrow)
        self.addChild(hawk)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let loc = touches.first?.locationInNode(self) {
            if let pathsButton = self.childNodeWithName("paths") {
                if pathsButton.containsPoint(loc) {
                    if let pathScene = PathScene(fileNamed: "PathScene") {
                        pathScene.scaleMode = .AspectFit
                        self.view?.presentScene(pathScene, transition: SKTransition.crossFadeWithDuration(1))
                    }
                }
            }
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        guard lastTime > 0 else {
            lastTime = currentTime
            return
        }
        let deltaTime = currentTime - lastTime
        lastTime = currentTime
        self.componentSystem.updateWithDeltaTime(deltaTime)
    }
}
